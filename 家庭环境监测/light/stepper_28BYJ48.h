#ifndef STEPPER_28BYJ48_H
#define STEPPER_28BYJ48_H

#include <Arduino.h>
void photosensitive();
class Stepper_28BYJ48 {
    public:
        Stepper_28BYJ48(int IN1, int IN2, int IN3, int IN4);
        void step(int);

    private:
        void setOutput(int out);
        int lookup[8] = {B01000, B01100, B00100, B00110, B00010, B00011, B00001, B01001};
        int _IN1; int _IN2; int _IN3; int _IN4; 
};

#endif
