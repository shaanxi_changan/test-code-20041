#ifndef MY_PHOTOSENSITIVE_H
#define MY_PHOTOSENSITIVE_H
#include "stepper_28BYJ48.h"
#include <stdio.h>
#include <stdlib.h>
typedef struct value_status
{
  double myvalue;
  int nowstatus;
}VS;
VS* photosensitive(VS *p);
VS* photosensitive2(VS *p);
#endif
