#include "my_photosensitive.h"
#include "stepper_28BYJ48.h"
VS* photosensitive(VS *p)
{
  Stepper_28BYJ48 stepper(D1, D2, D3, D4);
  static int flag=0;
  int sensorValue = analogRead(A0); 
  double transition = sensorValue * (5.0 / 1024.0);
  p->myvalue=transition;
  //0代表开着，1代表关着
  if(flag==0 && ((transition <=2.0)|| (transition>=4.95 && transition<=5.0)))
   {
    //Serial.println(transition);
    stepper.step(-512);
    //delay(1000);
    flag=1;
   }
   else if(flag==1 && (transition>=2.0 && transition<=4.95))
   {
    //Serial.println(transition);
    stepper.step(512);
    flag=0;
   }
   else if(transition>5.0)
   {
    //Serial.println(transition);
    Serial.println("error");
   }
   p->nowstatus=flag;
   return p;
  
}
