 #include "stepper_28BYJ48.h"
#include "my_photosensitive.h"
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include<ESP8266WiFi.h>
#include <Servo.h>
//#include <SHA256.h>
Stepper_28BYJ48 stepper(D1, D2, D3, D4);
#define  ws_wife_ssid   "shaanxi"
#define  ws_wife_password  "12345678"
#define  ws_mqtt_server  "a1ioVzl2LJf.iot-as-mqtt.cn-shanghai.aliyuncs.com"
#define  ws_mqtt_port  "1883"
#define  ws_mqtt_clientid  "10000|securemode=3,signmethod=hmacsha1|"
#define  ws_mqtt_user  "light&a1ioVzl2LJf"
#define  ws_mqtt_password  "DA2CC9C4F87BBD1B0DD447E34842998217092D7F"

//#define  WS_ALINK_TOPIC_PROP_POST  "/sys/a1nJAwK9nRv/my_light/thing/event/property/post"
//#define  WS_WEIXIN_ALINK_TOPIC_PROP_POST  "/sys/a1nJAwK9nRv/weixin/thing/service/property/set"
#define  WS_ALINK_TOPIC_PROP_POST  "/sys/a1ioVzl2LJf/light/thing/event/property/post"
#define  WS_WEIXIN_ALINK_TOPIC_PROP_POST "/sys/a1ioVzl2LJf/weixinduan/thing/service/property/set"
//#define  WS_ALINK_TOPIC_PROP_SET   "/sys/a1nJAwK9nRv/my_light/thing/service/property/set"//属性设置
#define  WS_ALINK_TOPIC_PROP_SET   "/a1ioVzl2LJf/light/user/light_set"//窗帘设置
//#define  WS_ALINK_TOPIC_PROP_MODEL   "/a1nJAwK9nRv/my_light/user/model_light"//模式设置
#define  WS_ALINK_TOPIC_PROP_MODEL   "/a1ioVzl2LJf/light/user/light_model"//模式设置
char ws_dataTemplete[]= "{\"id\":\"10000\",\"params\":{\"MeasuredIlluminance\":%.2lf,\"WorkState\":%d},\"method\":\"thing.event.property.post\"}";
//char ws_dataTemplete2[]= "{\"id\":\"989898\",\"params\":{\"MeasuredIlluminance\":%.2lf},\"method\":\"thing.event.property.post\"}";
char ws_dataTemplete2[]= "{\"id\":\"10000\",\"params\":{\"MeasuredIlluminance\":%.2lf,\"WorkState\":%d},\"method\":\"thing.event.property.post\"}";
char ws_msgJson[200];
char ws_msgJson2[200];
int ws_model_flag=0;
//char model_flag[20]="false";
WiFiClient ws_espClient;
PubSubClient ws_client(ws_espClient);
void callback(char* topic, byte* payload, unsigned int length)
{
   
    //Serial.print("Message arrived [");
    //Serial.print(topic);
    //Serial.print("] ");
   // payload[length] = '\0';
    //Serial.print("****** ");
    //Serial.println((char *)payload);
    //Serial.print("****** ");
    //Serial.println((char *)payload);
   
    if (strstr(topic, WS_ALINK_TOPIC_PROP_SET))
    {
        StaticJsonDocument<200> doc;
        deserializeJson(doc, payload);
         int params_LightSwitch = doc["subdata"]; 
           
        if(params_LightSwitch==false)
        {
          Serial.print(params_LightSwitch);
         //Serial.println("led off");
        //digitalWrite(LED, HIGH); 
        stepper.step(512);
        }
        else if(params_LightSwitch==true)
        {
          //Serial.print((char)root["set"]);
          Serial.print(params_LightSwitch);
          stepper.step(-512);       
        }
    }
    else if(strstr(topic, WS_ALINK_TOPIC_PROP_MODEL))
    {
      StaticJsonDocument<200> doc2;
        deserializeJson(doc2, payload);
         int params_LightSwitch2 = doc2["subdata"]; 
           
        if(params_LightSwitch2==true)
        {
          ws_model_flag=params_LightSwitch2;
          Serial.print(params_LightSwitch2);
        }
        else if(params_LightSwitch2==false)
        {
          ws_model_flag=params_LightSwitch2; 
          Serial.print(params_LightSwitch2);
        }
          
    }
    
}
void setup() {
  Serial.begin(115200); 
  WiFi.begin(ws_wife_ssid,ws_wife_password);
  while(!WiFi.isConnected())
  {
    delay(100);
  }
  Serial.println("WiFi is connected,IP = "+WiFi.localIP().toString());
  ws_client.setServer("a1ioVzl2LJf.iot-as-mqtt.cn-shanghai.aliyuncs.com",1883);
  ws_client.setCallback(callback);  
}
void reconnect()
{
  while (!ws_client.connected()) {
  Serial.print("Attempting MQTT connection...");
    if (ws_client.connect(ws_mqtt_clientid,ws_mqtt_user,ws_mqtt_password)) {
      Serial.println("connected");
      ws_client.subscribe(WS_ALINK_TOPIC_PROP_SET);
      ws_client.subscribe(WS_ALINK_TOPIC_PROP_MODEL);
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(ws_client.state());
      Serial.println(" try again in 1 seconds");
      delay(500);
    }
  }
}
void loop() {
    if (!ws_client.connected()) 
    {
    reconnect();
    }
    ws_client.loop();
    if(ws_model_flag)
    {
    memset(ws_msgJson2,0,200);
    VS* q=(VS*)malloc(sizeof(VS)*1);
    q=photosensitive2(q);
    snprintf(ws_msgJson2,200,ws_dataTemplete2,q->myvalue,q->nowstatus);
     Serial.println( q->myvalue);
    Serial.println(ws_msgJson2);
    free(q);
    Serial.println("**************");
    Serial.println(ws_model_flag);
    ws_client.publish(WS_ALINK_TOPIC_PROP_POST,(uint8_t* )ws_msgJson2,strlen(ws_msgJson2));
    }
    else
    {
    memset(ws_msgJson,0,200);
    VS* p=(VS*)malloc(sizeof(VS)*1);
    p=photosensitive(p);
    snprintf(ws_msgJson,200,ws_dataTemplete,p->myvalue,p->nowstatus);
    //Serial.println( p->myvalue);
    //Serial.println( p->nowstatus);
    Serial.println(ws_msgJson);
    free(p);
    //Serial.println("**************");
    Serial.println(ws_model_flag);
    ws_client.publish(WS_ALINK_TOPIC_PROP_POST,(uint8_t* )ws_msgJson,strlen(ws_msgJson));
    }
    delay(500);
    
}
