#include <ESP8266WiFi.h>
#include "mq.h"
#include <PubSubClient.h>
#include <ArduinoJson.h>

#define  mqtt_server  "a1ioVzl2LJf.iot-as-mqtt.cn-shanghai.aliyuncs.com"
#define  mqtt_port  "1883"
#define  mqtt_clientid  "10003|securemode=3,signmethod=hmacsha1|"
#define  mqtt_user  "mq_5&a1ioVzl2LJf"
#define  mqtt_password  "650F7DD6A8C06513B6EAF511140AF5496D4DBFF6"
#define  TOPIC_PROP_POST  "/sys/a1ioVzl2LJf/mq_5/thing/event/property/post"
#define ALINK_TOPIC_PROP_MODEL   "/a1ioVzl2LJf/mq_5/user/mq_setmodel"//报警模式设置
#define ALINK_TOPIC_PROP_SET "/a1ioVzl2LJf/mq_5/user/mq_sub"//报警控制



char data[]= "{\"id\":\"10003\",\"params\":{\"CombustibleGasCheck\":%f,\"AlarmSwitch\":%d},\"method\": \"thing.event.property.post\"}";
char data1[]= "{\"id\":\"10003\",\"params\":{\"CombustibleGasCheck\":%f,\"AlarmSwitch\":%d},\"method\": \"thing.event.property.post\"}";
char Json[200];
char Json2[200];
unsigned short json_len=0;
int model_flag=0;
WiFiClient espClient;
PubSubClient client(espClient);

void callback(char* topic, byte* payload, unsigned int length)
{
 if (strstr(topic, ALINK_TOPIC_PROP_SET))
    {
        StaticJsonDocument<200> doc;
        deserializeJson(doc, payload);
         int params_LightSwitch = doc["subdata"]; 
           
        if(params_LightSwitch==true)
        {
         Serial.print(params_LightSwitch);
         Serial.println("led on");
         digitalWrite(2,LOW);
        
       
        }
        else if(params_LightSwitch==false)
        {
          //Serial.print((char)root["set"]);
          Serial.print(params_LightSwitch);
         Serial.println("led off");
          digitalWrite(2,HIGH);
        }
    }
    else if(strstr(topic, ALINK_TOPIC_PROP_MODEL))
    {
      StaticJsonDocument<200> doc2;
        deserializeJson(doc2, payload);
         int params_LightSwitch2 = doc2["subdata"]; 
           
        if(params_LightSwitch2==true)
        {
          model_flag=params_LightSwitch2;
          Serial.print(params_LightSwitch2);
        }
        else if(params_LightSwitch2==false)
        {
          model_flag=params_LightSwitch2; 
          Serial.print(params_LightSwitch2);
        }
          
    }
}

void setup() {
  // put your setup code here, to run once:
  pinMode(2, OUTPUT);
  pinMode(A0, INPUT);
  digitalWrite(2,HIGH);
  Serial.begin(74880);
  WiFi.begin("shaanxi", "12345678");
  while(!WiFi.isConnected())
  {
    Serial.println("WiFi is connecting");
    delay(500);
  }
  Serial.println("WiFi is connected, IP is " + WiFi.localIP().toString());
  client.setServer("a1ioVzl2LJf.iot-as-mqtt.cn-shanghai.aliyuncs.com", 1883);
  client.setCallback(callback);
}

void reconnect()
{
  while(!client.connected())
  {
    Serial.print("MQTT connedting  ");
    if(client.connect(mqtt_clientid, mqtt_user, mqtt_password))
    {
      Serial.println("already connected");
	   client.subscribe(ALINK_TOPIC_PROP_SET);
      client.subscribe(ALINK_TOPIC_PROP_MODEL);
    }else{
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(500);
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  if(!client.connected())
  {
    reconnect(); 
  }
  client.loop();

  if(model_flag)
    {
     MQ* q = (MQ *)malloc(sizeof(MQ) * 1);
  q= getMQ1(q);
  Serial.println(q->m);
  Serial.println(q->n);
    memset(Json2,0,200);
  snprintf(Json2,200,data1,q->m,q->n);
  Serial.println(Json2);
  Serial.println(model_flag);
  client.publish(TOPIC_PROP_POST,(uint8_t *)Json2,strlen(Json2));
  free(q);
  delay(500);
  
    }
    else
    {
   MQ* p = (MQ *)malloc(sizeof(MQ) * 1);
  p = getMQ(p);
  Serial.println(p->m);
  Serial.println(p->n);
  memset(Json,0,200);
  snprintf(Json,200,data,p->m,p->n);
  Serial.println(model_flag);
  client.publish(TOPIC_PROP_POST,(uint8_t *)Json,strlen(Json));
  free(p);
  delay(500);
    }
  
}
