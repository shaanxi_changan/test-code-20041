#ifndef _MQ_H
#define _MQ_H
#include <ESP8266WiFi.h>
typedef struct myMQ
  {
    float m;//保存传感器数值
    int n;//保存led灯状态,灯亮为1,灭0
  }MQ;
//函数名：getMQ
//参数：无
//返回值：MQ*
MQ * getMQ(MQ * p);
MQ * getMQ1(MQ * q);


#endif
