
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <DHT.h>

#define  wife_ssid   "shaanxi"
#define  wife_password  "12345678"
#define  mqtt_server  "a1ioVzl2LJf.iot-as-mqtt.cn-shanghai.aliyuncs.com"
#define  mqtt_port  "1883"
#define  mqtt_clientid  "10002|securemode=3,signmethod=hmacsha1|"
#define  mqtt_user  "DHT_&a1ioVzl2LJf"
#define  mqtt_password  "4D5E0605CA9C4B336FE842A3D6A203A78DBC7290"
#define pin_led 16
#define ALINK_TOPIC_PROP_POST "/sys/a1ioVzl2LJf/DHT_/thing/event/property/post"
#define ALINK_TOPIC_PROP_MODEL   "/a1ioVzl2LJf/DHT_/user/wenshiduset_model"//空调模式设置
#define ALINK_TOPIC_PROP_SET "/a1ioVzl2LJf/DHT_/user/wenshidu_sub"//空调控制
int model_flag=0;
DHT sensor(D2, DHT11);
long lastMsg=0;
int i;
int j;
char dataTemplete[]= "{\"id\":\"10002\",\"params\":{\"CurrentTemperature\":%d,\"CurrentHumidity\":%d,\"E_Status_UP\":%d},\"method\": \"thing.event.property.post\"}";
char dataTemplete2[]= "{\"id\":\"10002\",\"params\":{\"CurrentTemperature\":%d,\"CurrentHumidity\":%d,\"E_Status_UP\":%d},\"method\": \"thing.event.property.post\"}";
char msgJson[200];
char msgJson2[200];
unsigned short json_len=0;
uint8_t debug_buffer_start_index=0;
int p1_0=0;
byte temp;//用于存放传感器温度的数值
byte humi;//湿度
WiFiClient espClient;
PubSubClient hf_client(espClient);

void callback(char* topic, byte* payload, unsigned int length) {
  if (strstr(topic, ALINK_TOPIC_PROP_SET))
    {
        StaticJsonDocument<200> doc;
        deserializeJson(doc, payload);
         int params_LightSwitch = doc["subdata"]; 
           
        if(params_LightSwitch==false)
        {
          Serial.print(params_LightSwitch);
         Serial.println("led off");
         digitalWrite(16,HIGH);
       
        }
        else if(params_LightSwitch==true)
        {
          //Serial.print((char)root["set"]);
          Serial.print(params_LightSwitch);
         Serial.println("led on");
         digitalWrite(16,LOW);
        }
    }
    else if(strstr(topic, ALINK_TOPIC_PROP_MODEL))
    {
      StaticJsonDocument<200> doc2;
        deserializeJson(doc2, payload);
         int params_LightSwitch2 = doc2["subdata"]; 
           
        if(params_LightSwitch2==true)
        {
          model_flag=params_LightSwitch2;
          Serial.print(params_LightSwitch2);
        }
        else if(params_LightSwitch2==false)
        {
          model_flag=params_LightSwitch2; 
          Serial.print(params_LightSwitch2);
        }
          
    }
}
void setup() {
  // put your setup code here, to run once:
  pinMode(16,OUTPUT);
  //digitalWrite(16,LOW);
  Serial.begin(74880);
  Serial.println();
  Serial.print("Connetct to");
  Serial.println(wife_ssid);
  WiFi.begin(wife_ssid,wife_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".....");
  }
  Serial.println("IP address:");
  Serial.println(WiFi.localIP());
  hf_client.setServer("a1ioVzl2LJf.iot-as-mqtt.cn-shanghai.aliyuncs.com",1883); 
  sensor.begin();
  hf_client.setCallback(callback);
  
}
void reconnect()
{
  while (!hf_client.connected()) {
  Serial.print("Attempting MQTT connection...");
    if (hf_client.connect(mqtt_clientid,mqtt_user,mqtt_password)) {
      Serial.println("connected");
      hf_client.subscribe(ALINK_TOPIC_PROP_SET);
      hf_client.subscribe(ALINK_TOPIC_PROP_MODEL);
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(hf_client.state());
      Serial.println(" try again in 5 seconds");
      delay(500);
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  if (!hf_client.connected()) 
  {
    reconnect();
  }
  hf_client.loop();
  temp = sensor.readTemperature();
  humi = sensor.readHumidity();
  if(model_flag)
  {
  j=0;
   snprintf(msgJson2,200,dataTemplete2,temp,humi,j);
  Serial.println(msgJson2);
  Serial.println(model_flag);
  hf_client.publish(ALINK_TOPIC_PROP_POST,(uint8_t *)msgJson2,strlen(msgJson2)); 
    
  }
  else
  {
  if(sensor.readHumidity()>80||sensor.readHumidity()<40||sensor.readTemperature()>30||sensor.readTemperature()<20)
  {
    digitalWrite(16,LOW);
    i=1;
    delay(500);
    }
    else {
      digitalWrite(16,HIGH);
      i=0;
      };
  memset(msgJson,0,200);
  snprintf(msgJson,200,dataTemplete,temp,humi,i);
  //String s = + "温度" + String(temp, 1) + "," + "湿度" + String(humi, 0);
  //Serial.println(s);
  Serial.println(msgJson);
  hf_client.publish(ALINK_TOPIC_PROP_POST,(uint8_t *)msgJson,strlen(msgJson));
  }
  delay(500);
}
